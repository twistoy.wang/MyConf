#!/bin/bash

check_cmd() {
  command -v "$1" > /dev/null 2>&1
  return $?
}

__check_rust_installed() {
  if check_cmd "rustup"; then
    echo "Rust exists. Updating..."
    rustup update
    rustup component add rls-preview rust-analysis rust-src
  else
    echo "Rust not exists. Installing...."
    curl https://sh.rustup.rs -sSf | sh
    rustup component add rls-preview rust-analysis rust-src
  fi
}

__check_nvm_installed() {
  if ! check_cmd "nvm"; then
    echo "NVM not exists. Installing..."
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
  else
    echo "NVM exists."
  fi
  nvm install 10.13
  nvm use 10.13
  npm install -g yarn
}

install_required_tools() {
  __check_rust_installed
  __check_nvm_installed

  cargo install exa
  cargo install ripgrep
  cargo install fd-find
  cargo install bat

  brew install diff-so-fancy
  brew install tldr

  brew install fzf
  $(brew --prefix)/opt/fzf/install

  brew install htop
  brew install ncdu
}


install_required_tools
