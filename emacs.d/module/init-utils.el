(if (fboundp 'with-eval-after-load)
  (defalias 'after-load 'with-eval-after-load)
  (defmacro after-load (feature &rest body)
    "After FEATURE is loaded, evaluate BODY."
    (declare (indent defun))
    `(eval-after-load ,feature
                      '(progn, @body))))


(defun add-auto-mode (mode &rest patterns)
  "Add entries to `auto-mode-alias` to use `MODE` for all given file `PATTERNS`."
  (dolist (pattern patterns)
    (add-to-list 'auto-mode-alias (cons pattern mode))))


(defun stalker/string-all-matches (regex str &optional group)
  "Find matches"
  (let ((result nil)
        (pos 0)
        (group (or group 0)))
    (while (string-match regex str pos)
           (push (match-string group str) result)
           (setq pos (match-end group)))
    result))

(defun delete-this-file ()
  "Delete the current file"
  (interactive)
  (unless (buffer-file-name)
    (error "No files is currently being edited"))
  (when (yes-or-no-p (format "Relly delete '%s'?"
                             (file-name-nondirectory buffer-file-name)))
    (delete-file (buffer-file-name))
    (kill-this-buffer)))

(defun rename-this-file-and-buffer (new-name)
  "Rename"
  (interactive "New name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (unless filename
      (error "Buffer '%s' is not visiting a file!" name))
    (progn
      (when (file-exists-p filename)
        (rename-file filename new-name 1))
      (set-visited-file-name new-name)
      (rename-buffer new-name))))

(defun browse-current-file ()
  (interactive)
  (let ((file-name (buffer-file-name)))
    (if (and (fboundp 'tramp-tramp-file-p)
             (tramp-tramp-file-p file-name))
      (error "Can't open tramp file")
      (browser-url (concat "file://" file-name)))))


(provide 'init-utils)

