;; LSP
(require-package 'lsp-mode)

;; LSP UI
(require-package 'lsp-ui)
(add-hook 'lsp-mode-hook 'lsp-ui-mode)

;; Company lsp
(require-package 'company-lsp)
(after-load 'company
            (lambda () (stalker/local-push-company-backend 'company-lsp)))

;; ccls
(require-package 'ccls)
(setq ccls-executable "/usr/local/bin/ccls")
(setq ccls-extra-init-params '(:cacheFormat "msgpack"))

(with-eval-after-load 'projectile
  (setq projectile-project-root-files-top-down-recurring
        (append '("compile_commands.json"
                  ".ccls")
                projectile-project-root-files-top-down-recurring)))

(defun ccls//enable ()
  (condition-case nil
      (lsp-ccls-enable)
    (user-error nil)))

(use-package ccls
  :commands lsp-ccls-enable
  :init
  (add-hook 'c-common-hook #'ccls//enable)
  (add-hook 'c++-common-hook #'ccls//enable)
  )
(setq ccls-extra-init-params '(:index (:comments 2) :completion (:detailedLabel t)))
(setq ccls-extra-init-params '(:completion (:detailedLabel t)))
(setq company-transformers nil company-lsp-async t company-lsp-cache-candidates nil)

(lsp-find-custom "$ccls/base")
(lsp-find-custom "$ccls/callers")
; Use lsp-goto-implementation or lsp-ui-peek-find-implementation for derived types/functions
(lsp-find-custom "$ccls/vars")

;; Alternatively, use lsp-ui-peek interface
(lsp-ui-peek-find-custom 'base "$ccls/base")
(lsp-ui-peek-find-custom 'callers "$ccls/callers")

(defun ccls/vars (kind) (lsp-ui-peek-find-custom 'vars "$ccls/vars" (plist-put (lsp--text-document-position-params) :kind kind)))
(ccls/vars 3) ;; field or local variable
(ccls/vars 1) ;; field
(ccls/vars 4) ;; parameter
(defun ccls/bases ()
  (interactive)
  (lsp-ui-peek-find-custom 'base "$ccls/inheritanceHierarchy"
                           (append (lsp--text-document-position-params) '(:flat t :level 3))))
(defun ccls/derived ()
  (interactive)
  (lsp-ui-peek-find-custom 'derived "$ccls/inheritanceHierarchy"
                           (append (lsp--text-document-position-params) '(:flat t :level 3 :derived t))))
(defun ccls/members ()
  (interactive)
  (lsp-ui-peek-find-custom 'base "$ccls/memberHierarchy"
                           (append (lsp--text-document-position-params) '(:flat t))))

(setq ccls-sem-highlight-method 'font-lock)
;; alternatively, (setq ccls-sem-highlight-method 'overlay)

;; For rainbow semantic highlighting
(ccls-use-default-rainbow-sem-highlight)

(provide 'init-cpp)

