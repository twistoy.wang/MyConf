(require 'package)

(add-to-list 'package-archives '("melpa" . "http://elpa.emacs-china.org/melpa/"))
(add-to-list 'package-archives '("gnu"   . "http://elpa.emacs-china.org/gnu/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))


(defvar stalker/required-packages nil)

(defun require-package (package &optional min-version no-refresh)
  (let ((available
          (or (package-installed-p package min-version)
              (if (or (assoc package package-archive-contents) no-refresh)
                (package-install package)
                (progn
                  (package-refresh-contents)
                  (require-package package min-version t))))))
    (prog1 available
      (when (and available (boundp 'package-selected-packages))
        (add-to-list 'stalker/required-packages package)))))

(defun maybe-require-package (package &optional min-version no-refresh)
  (condition-case err
                  (require-package package min-version no-refresh)
                  (error
                    (message "Couldn't install optional package `%s': %S" package err)
                    nil)))

(setq package-enable-at-startup nil)
(package-initialize)


(when (fboundp 'package--save-selected-packages)
  (require-package 'seq)
  (add-hook 'after-init-hook
            (lambda () (package--save-selected-packages
                         (seq-uniq (append stalker/required-packages package-selected-packages))))))

(require-package 'fullframe)
(fullframe list-packages quit-window)


(require-package 'cl-lib)
(require 'cl-lib)

(defun stalker/set-tabulated-list-column-width (col-name width)
  (when (> width (length col-name))
    (cl-loop for column across tabulated-list-format
             when (string= col-name (car column))
             do (setf (elt column 1) width))))

(defun stalker/maybe-widen-package-menu-columns ()
  (when (boundp 'tabulated-list-format)
    (stalker/set-tabulated-list-column-width "Version" 13)
    (let ((longest-archive-name (apply 'max (mapcar 'length (mapcar 'car package-archives)))))
      (stalker/set-tabulated-list-column-width "Archive" longest-archive-name))))

(add-hook 'package-menu-mode-hook 'stalker/maybe-widen-package-menu-columns)

(provide 'init-elpa)

