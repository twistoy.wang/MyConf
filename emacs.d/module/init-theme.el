(require-package 'color-theme-sanityinc-solarized)
(require-package 'color-theme-sanityinc-tomorrow)
(require-package 'zenburn-theme)

(defun apply-theme ()
  (load-theme 'zenburn t))

(add-hook 'after-init-hook 'apply-theme)

(provide 'init-theme)
