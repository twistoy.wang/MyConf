(setq debug-on-error t)

(add-to-list 'load-path (expand-file-name "module" user-emacs-directory))

;;; global check
(defconst *spell-check-support-enabled* nil)
(defconst *is-a-mac* (eq system-type 'darwin))

;;; garbage collection settings
(let ((init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold (* 20 1024 1024)))))
;;(let ((init-gc-cons-threshold (* 128 1024 1024)))
;;  (setq gc-cons-threshold init-gc-cons-threshold)
;;  (add-hook 'emacs-startup-hook
;;            (lambda () (setq gc-cons-threshold (* 20 1024 1024))))


(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

;; packages
(require 'init-utils)
(require 'init-elpa)
(require 'init-exec-path)

(require-package 'scratch)
(require-package 'diminish)
(require-package 'use-package)

(require 'init-theme)
(require 'init-company)
(require 'init-cpp)

;; fond settings
(set-default-font "Fira Code")
(set-face-attribute 'default nil :height 120)

;;; server
(require 'server)
(unless (server-running-p)
  (server-start))


;;; load custom settings
(when (file-exists-p custom-file)
  (load custom-file))


(provide 'init)

