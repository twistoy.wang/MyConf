Stalker - A Simple neovim configuration
=======================================

Available Components
====================

Group: default
--------------

### stalker
Default component, enable all time

Plugins:
- liuchengxu/vim-which-key
- skywind3000/asyncrun.vim
- mhinz/vim-startify
- scrooloose/nerdtree
- Xuyuanp/nerdtree-git-plugin
- junegunn/fzf
- junegunn/fzf.vim
- vim-airline/vim-airline
- tpope/vim-surround
- tpope/vim-fugitive
- easymotion/vim-easymotion
- liuchengxu/space-vim-dark
- tpope/vim-vividchalk
- altercation/vim-colors-solarized
- vim-scripts/wombat256.vim

### Better
Plugins:
- dominikduda/vim_current_word
- matze/vim-move
- godlygeek/tabular
- junegunn/vim-easy-align
- scrooloose/nerdcommenter
- sheerun/vim-polyglot
- Yggdroot/indentLine
- tpope/vim-repeat
- terryma/vim-multiple-cursors
- tpope/vim-speeddating
- bogado/file-line
- Raimondi/delimitMate
- Shougo/denite.nvim

### term
Plugins:
- kassio/neoterm

Group: enhance
--------------
### bookmark
Plugins:
- MattesGroeger/vim-bookmarks

### snippets
Plugins:
- SirVer/ultisnips

Use `SirVer/ultisnips` as the snippets engine with some default snippets.

### textobj
Plugins:
- kana/vim-textobj-user
- kana/vim-textobj-entire
- kana/vim-textobj-indent
- lucapette/vim-textobj-underscore

### themes
Plugins:
- joshdick/onedark.vim
- KeitaNakamura/neodark.vim
- rakr/vim-one
- challenger-deep-theme/vim

Group: lang
-----------
### cpp
Plugins:
- rhysd/vim-clang-format
- octol/vim-cpp-enhanced-highlight
- derekwyatt/vim-fswitch

### docker
Plugins:
- ekalinin/Dockerfile.vim

### rust
- rust-lang/rust.vim

Group: lsp
----------
### coc
Plugins:
- neoclide/coc.nvim
- Shougo/neco-vim


Stalker will install `coc.nvim` use `yarn install`, so `yarn` is required.

### lcn
Plugins:
- autozimu/LanguageClient-neovim
- Shougo/deoplete.nvim
- Shougo/echodoc.vim

`Shougo/deoplete.nvim` requires `python3` support. Use:
```
> python3 -m "pip" install neovim --user
```
Group: tools
------------
### dash
Plugins:
- rizzatti/dash.vim

### git
Plugins:
- lambdalisue/gina.vim
- mhinz/vim-signify

### transpose
Plugins:
- salsifis/vim-transpose

