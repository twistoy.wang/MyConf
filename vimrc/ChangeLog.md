# Changelog

* 2018-11-29 feature(leader): Use `vim-which-key` instead of `leaderGuide`, add key hints
* 2018-11-29 fix(init): Enable swapfile
* 2018-11-28 fix(default): Toggle quickfix

