UsePlug 'liuchengxu/vim-which-key'
UsePlug 'skywind3000/asyncrun.vim'
UsePlug 'mhinz/vim-startify'

UsePlug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }
UsePlug 'Xuyuanp/nerdtree-git-plugin', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }

UsePlug 'junegunn/fzf',  { 'dir': '~/.fzf', 'do': './install --all', 'on': [] }
UsePlug 'junegunn/fzf.vim', { 'on': [] }

UsePlug 'vim-airline/vim-airline'
UsePlug 'tpope/vim-surround'
UsePlug 'tpope/vim-fugitive'

UsePlug 'easymotion/vim-easymotion'

UsePlug 'liuchengxu/space-vim-dark'
UsePlug 'tpope/vim-vividchalk'
UsePlug 'altercation/vim-colors-solarized'
UsePlug 'vim-scripts/wombat256.vim'

call timer_start(100, 'stalker#defer#whichkey')
call timer_start(700, 'stalker#defer#fzf')
