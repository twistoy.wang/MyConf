scriptencoding utf-8

" default theme setting
" let g:solarized_termcolors = 256
syntax enable
" set background=dark
silent! color wombat256mod

augroup SBasic
  autocmd!
  autocmd BufReadPre *
        \ if getfsize(expand("%")) > 10000000
        \|  syntax off
        \|endif

  " Restore cursor position when opening file
  autocmd BufReadPost *
        \ if line("'\"") > 1 && line("'\"") <= line("$")
        \|  execute "normal! g`\""
        \|endif

  autocmd BufReadPost *
        \ if line('$') > 1000
        \|  silent! set norelativenumber
        \|endif

  " http://vim.wikia.com/wiki/Speed_up_Syntax_Highlighting
  autocmd BufEnter * :syntax sync maxlines=200

  autocmd BufEnter * call MyLastWindow()
  function! MyLastWindow()
    " if the window is quickfix/locationlist
    let l:bt_blacklist = ['quickfix', 'locationlist']
    let l:ft_blocklist = ['quickmenu']
    if index(l:bt_blacklist, &buftype) >= 0 || index(l:ft_blocklist, &filetype) >= 0
      " if this window is last on screen quit without warning
      if winnr('$') == 1
        quit!
      endif
    endif
    if (winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree()) | q! | endif
  endfunction
augroup END


""" Show trailing white space
hi ExtraWhitespace guifg=#FF2626 gui=underline ctermfg=124 cterm=underline
match ExtraWhitespace /\s\+$/

" vim-startify settings {{{

let g:startify_custom_header = g:stalker#default#startify#header
let g:startify_list_order = g:stalker#default#startify#list_order
let g:startify_change_to_vcs_root = 1

"

" mapleader and for vim-which-key {{{

let g:mapleader = "\<Space>"
let g:maplocalleader = ','
let g:which_key_timeout = 500
nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>
call stalker#default#keybindings#trigger()

" }}}

" rainbow settings {{{

let g:rainbow_active = 1
autocmd FileType c,cpp,java,php autocmd BufWritePre <buffer> %s/\s\+$//e
command! -bang -nargs=* -complete=file Make AsyncRun -program=make @ <args>

" }}}

let $LANG = 'en_US'
let g:fzf_colors = g:stalker#default#fzf#colors

cmap w!! w !sudo tee > /dev/null %

if !exists('g:airline_powerline_fonts')
  let g:airline_left_sep=''
  let g:airline_right_sep=''

  let g:airline_synbols = {}
  let g:airline_synbols.linenr = '␊'
  let g:airline_synbols.linenr = '␤'
  let g:airline_synbols.linenr = '¶'
  let g:airline_synbols.branch = '⎇'
  let g:airline_synbols.paste = 'Þ'
  let g:airline_synbols.whitespace = 'Ξ'
endif

let g:should_asyncrun_airline_init = 0
let g:airline_section_error = airline#section#create_right(['%{g:asyncrun_status}'])
" }

set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

set expandtab

" scrooloose/nerdtree {
let g:NERDTreeShowHidden = 1
let g:NERDTREEAutoDeleteBuffer = 1
let g:NERDTreeDirArrowCollapsible = "~"
let g:NERDTreeDirArrowExpandable = "\u276f"

augroup loadNerdTree
  autocmd!
  autocmd VimEnter * silent! autocmd! FileExplorer
  autocmd BufEnter, BufNew *
      \ if isdirectory(expand('<amatch>'))
      \   call plug#load('nerdtree')
      \   call nerdtree#checkForBrowser(expand('<amatch>'))
      \ endif
augroup END
" }

set tabstop=2 shiftwidth=2 autoindent expandtab

" disable auto comment
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" disable surround default keybindings {{{

let g:surround_no_mappings = 0
let g:surround_no_insert_mappings = 0

" }}}

" auto-move quickfix botright {{{

autocmd FileType qf wincmd J

" "}}}
