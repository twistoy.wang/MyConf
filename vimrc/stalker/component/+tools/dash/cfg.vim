" keybinding
if g:is_mac
  nmap <silent> <leader>sd <Plug>DashSearch
  call stalker#keybinding#reg_leader('sd', 'dash-search')
  nmap <silent> <leader>sD <Plug>DashGlobalSearch
  call stalker#keybinding#reg_leader('sD', 'dash-global-search')
else
  let g:dasht_filetype_docsets = {}
  let g:dasht_filetype_docsets['cpp'] = ['^c$', '^c++$', 'boost', 'OpenGL']
  let g:dasht_filetype_docsets['python'] = ['(num|sci)py', 'pandas']
  let g:dasht_filetype_docsets['html'] = ['css', 'js', 'bootstrap']

  let g:dasht_results_window = 'topleft new'

  nnoremap <silent> <leader>sd :call Dasht([expand('<cword>'), expand('<cWORD>')])<CR>
	nnoremap <silent> <leader>sD :call Dasht([expand('<cword>'), expand('<cWORD>')], '!')<CR>
  call stalker#keybinding#reg_leader('sd', 'dash-search')
  call stalker#keybinding#reg_leader('sD', 'dash-global-search')

  function! s:InputAndSearchInDash() abort
    let l:wd = input('Search in Dasht: ')
    call Dasht(l:wd, '!')
  endfunction
  nnoremap <silent> <leader>si :call <SID>InputAndSearchInDash()<CR>
  call stalker#keybinding#reg_leader('si', 'dash-search-input')
endif
