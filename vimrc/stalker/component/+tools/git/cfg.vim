" keybindings {{{

nnoremap <silent> <leader>vs :Gina status --opener=101split<CR>
call stalker#keybinding#reg_leader('vs', 'git-status')
nnoremap <silent> <leader>va :Gina add %<CR>
call stalker#keybinding#reg_leader('va', 'git-add-file')
nnoremap <silent> <leader>vd :Gina diff<CR>
call stalker#keybinding#reg_leader('vd', 'git-diff')

" }}}


