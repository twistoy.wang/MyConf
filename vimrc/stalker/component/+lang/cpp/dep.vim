UsePlug 'rhysd/vim-clang-format',           { 'for': [ 'c', 'cpp' ] }
UsePlug 'octol/vim-cpp-enhanced-highlight', { 'for': [ 'c', 'cpp' ] }
UsePlug 'derekwyatt/vim-fswitch',           { 'for': [ 'c', 'cpp' ] }
