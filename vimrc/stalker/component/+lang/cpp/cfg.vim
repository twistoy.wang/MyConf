function! s:keybind_cpp() abort
  nnoremap <silent> <leader>fa :FSHere<CR>
  call stalker#keybinding#reg_leader('fa', 'switch-here')
  nnoremap <silent> <leader>fc :ClangFormat<CR>
  call stalker#keybinding#reg_leader('fc', 'clang-format')
  nnoremap <silent> <leader>fv :FSSplitRight<CR>
  call stalker#keybinding#reg_leader('fs', 'switch-split-right')
endfunction

autocmd FileType c,cpp call s:keybind_cpp()
