" keybinding
nnoremap <silent><leader>mr :Denite lmake-all-targets<CR>
call stalker#keybinding#reg_leader('mr', 'denite-all-targets(lm)')

" Generate makefile
function! s:generate_makefile() abort
  let root_path = _lbuild_get_build_root()
  let build_file = _lbuild_current_build(1)
  execute 'AsyncRun -cwd=' . root_path . ' ./gen_makefile.sh ' . build_file . ' -m32'
  execute 'botright copen'
endfunction

nnoremap <silent><leader>mg :call <SID>generate_makefile()<CR>
call stalker#keybinding#reg_leader('mg', 'gen-makefile(lm)')

" Do make
function! s:do_make(target) abort
  let root_path = _lbuild_get_build_root()
  execute 'AsyncRun -cwd=' . root_path . ' make ' . a:target . ' -j8'
  execute 'botright copen'
endfunction

nnoremap <silent><leader>mm :call <SID>do_make('release')<CR>
nnoremap <silent><leader>mt :call <SID>do_make('test')<CR>
call stalker#keybinding#reg_leader('mm', 'make-release(lm)')
call stalker#keybinding#reg_leader('mt', 'make-test(lm)')
