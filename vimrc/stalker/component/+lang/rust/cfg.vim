function! s:keybinding_rust() abort
  nnoremap <silent> <leader>fc :call rustfmt#Format()<CR>
  call stalker#keybinding#reg_leader('fc', 'rust-format')
endfunction

autocmd FileType rust call s:keybinding_rust()
