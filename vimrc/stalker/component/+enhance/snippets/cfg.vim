set runtimepath+=$HOME/.stalker.vim/component/+enhance/snippets/snippets

let g:UltiSnipsJumpForwardTrigger="<c-f>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:UltiSnipsExpandTrigger="<c-space>"

