if has('nvim') || has("termguicolors")
  set termguicolors
endif

if exists('g:loaded_neodark') && g:loaded_neodark 
  let g:neodark#user_256color = 1
  let g:neodark#terminal_transparent = 1
endif
