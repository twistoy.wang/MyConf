let g:bookmark_auto_save = 1
let g:bookmark_manage_per_buffer = 1
let g:bookmark_highlight_lines = 1

let g:bookmark_no_default_key_mappings  = 1

function! BookmarkMappingKeys() abort
  nnoremap <silent> mm :BookmarkToggle<CR>
  nnoremap <silent> mi :BookmarkAnnotate<CR>
  nnoremap <silent> mn :BookmarkNext<CR>
  nnoremap <silent> mp :BookmarkPrev<CR>
  nnoremap <silent> ma :BookmarkShowAll<CR>
  nnoremap <silent> mc :BookmarkClear<CR>
  nnoremap <silent> mx :BookmarkClearAll<CR>
  nnoremap <silent> mkk :BookmarkMoveUp
  nnoremap <silent> mjj :BookmarkMoveDown
endfunction

function! BookmarkUnmappingKeys() abort
  unmap mm
  unmap mi
  unmap mn
  unmap mp
  unmap ma
  unmap mc
  unmap mx
  unmap mkk
  unmap mjj
endfunction

autocmd BufEnter * :call BookmarkMappingKeys()
autocmd BufEnter NERD_tree_* :call BookmarkUnmappingKeys()
