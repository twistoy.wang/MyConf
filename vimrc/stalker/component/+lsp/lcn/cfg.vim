""" settings for 'deoplete' {

let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_delay = 0

""" }

""" settings for 'Shougo/echodoc.vim' {

let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'signature'
set noshowmode

""" }

let g:min_pattern_length = 2
let g:LanguageClient_selectionUI = 'quickfix'
let g:LanguageClient_loadSettings = 1
let g:LanguageClient_settingsPath = $HOME . '/.config/nvim/settings.json'

set signcolumn=yes
call deoplete#custom#source('LanguageClient', 'min_pattern_length', 2)

nnoremap <silent> <leader>gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <leader>gD :call LanguageClient#textDocument_definition({'gotoCmd': 'split'})<CR>
nnoremap <silent> <leader>gr :call LanguageClient_textDocument_references()<CR>
nnoremap <silent> <leader>gi :call LanguageClient_textDocument_implementation()<CR>
nnoremap <silent> <leader>gR :call LanguageClient_textDocument_rename()<CR>
nnoremap <silent> <leader>gs :call LanguageClient_textDocument_documentSymbol()<CR>
nnoremap <silent> <leader>gS :call LanguageClient_workspace_symbol()<CR>
nnoremap <silent> <leader>gt :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> <leader>gf :call LanguageClient_textDocument_codeAction()<CR>
nnoremap <silent> <leader>gc :call LanguageClient_textDocument_completion()<CR>

let g:ulti_expand_res = 0 "default value, just set once
function! g:CompleteSnippet() abort
  if empty(v:completed_item)
    return
  endif

  call UltiSnips#ExpandSnippet()
  if g:ulti_expand_res > 0
    return
  endif
  
  let l:complete = type(v:completed_item) == v:t_dict ? v:completed_item.word : v:completed_item
  let l:comp_len = len(l:complete)

  let l:cur_col = mode() == 'i' ? col('.') - 2 : col('.') - 1
  let l:cur_line = getline('.')

  let l:start = l:comp_len <= l:cur_col ? l:cur_line[:l:cur_col - l:comp_len] : ''
  let l:end = l:cur_col < len(l:cur_line) ? l:cur_line[l:cur_col + 1 :] : ''

  call setline('.', l:start . l:end)
  call cursor('.', l:cur_col - l:comp_len + 2)

  call UltiSnips#Anon(l:complete)
endfunction

augroup lspCompleteDonw
  autocmd!

  autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
  autocmd! CompleteDone * call g:CompleteSnippet()
augroup END

imap <expr> <CR> (pumvisible() ? "\<C-Y>" : "\<CR>")

let g:LanguageClient_hasSnippetSupport = 1
let g:UltiSnipsExpandTrigger="<NUL>"
let g:UltiSnipsListSnippets="<NUL>"
" let g:UltiSnipsExpandTrigger = "<Plug>(ultisnips_expand)"
let g:UltiSnipsJumpForwardTrigger = "<C-f>"
let g:UltiSnipsJumpBackwardTrigger = "<C-b>"


set cmdheight=2

