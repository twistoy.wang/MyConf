let g:stalker_dir = $HOME.'/.stalker.vim'
let g:stalker_core_dir = '/core'

let g:stalker_version = "2018.12.1"
lockvar g:stalker_version

set runtimepath+=$HOME/.stalker.vim/core

filetype off
filetype plugin indent on

call stalker#begin()

" Only Entrance

Component 'stalker'

call stalker#end()

