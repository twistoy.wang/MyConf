function! stalker#component#has_loaded(component) abort
  return index(g:load_components, a:component) > -1 ? 1 : 0
endfunction

" Component environment initialize
" 
" find all components, save them
function! stalker#component#initialize() abort
  let l:cat_root = g:stalker_dir . '/component'
  " find all directories
  let l:cat_dirs = split(globpath(l:cat_root, '*'), '\n')
  " filter components
  let l:cat_path = filter(l:cat_dirs, 'isdirectory(v:val)')

  let l:cats = deepcopy(l:cat_path)

  let cat2components = {}

  for l:cat in l:cats
    let l:components_ = split(globpath(l:cat, '*'), '\n')
    let l:components = deepcopy(l:components_)
    let l:components_name = map(l:components, 'fnamemodify(v:val, ":t")')
    let l:cat_name = fnamemodify(l:cat, ':t')

    let l:cat2components[l:cat_name] = l:components_name

    for l:com in l:components_
      let l:k = fnamemodify(l:com, ':t')
      let g:component_available[l:k] = { 'dir': l:com }
    endfor
  endfor
endfunction


" Load configs in all enabled components
function! stalker#component#load_configs() abort
  for l:com in g:component_enabled
    call stalker#utils#source_file(g:component_available[l:com].dir . '/cfg.vim')
  endfor

  if exists('g:private')
    for l:com in g:private
      let l:com_cfg_file = g:stalker_dir . '/private/' . l:com . '/cfg.vim'

      if filereadable(expand(l:com_cfg_file))
        call stalker#utils#source_file(l:com_cfg_file)
      endif
    endfor
  endif

  let l:private_cfg = g:stalker_dir . '/private/cfg.vim'
  if filereadable(expand(l:private_cfg))
    call stalker#utils#source_file(l:private_cfg)
  endif
endfunction


