function! s:defer_load(...)
  for l:plug in a:000
    silent! call plug#load(l:plug)
  endfor
endfunction


function! stalker#defer#fzf(timer) abort
  call s:defer_load('fzf', 'fzf.vim')
endfunction

function! stalker#defer#whichkey(timer) abort
  call stalker#keybinding#build()
endfunction
