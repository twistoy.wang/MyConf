function! stalker#utils#download_vim_plug(plug_path) abort
  echo '====> Downloading vim-plug...'
  execute '!curl -fLo ' . a:plug_path . ' --create-dirs '
        \ 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endfunction

function! stalker#utils#plug_is_dir(plug_name) abort
  return isdirectory(expand(g:stalker_plug_root . a:plug_name)) ? 1 : 0
endfunction

function! stalker#utils#source_file(file) abort
  try
    execute 'source ' . fnameescape(a:file)
  catch
    call stalker#component#initialize()
  endtry
endfunction

function! stalker#utils#install_missing_plugs() abort
  augroup checkPlug
    autocmd!
    autocmd VimEnter *
      \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
      \|   echom '[stalker] Some components need to install the missing plugins first!'
      \|   PlugInstall --sync | q
      \| endif
  augroup END
endfunction

function! stalker#utils#merge_dict(expr1, expr2)
  if type(a:expr1) != type({}) || type(a:expr2) != type({})
    throw 'Error'
  endif

  for key in keys(a:expr2)
    if !has_key(a:expr1, key)
      let a:expr1[key] = a:expr2[key]
      continue
    endif

    let exp1 = a:expr1[key]
    let exp2 = a:expr2[key]

    if type(exp1) == type(exp2) && type(exp1) == type([])
      call extend(exp1, exp2)
      continue
    endif

    if type(exp1) == type(exp2) && type(exp1) == type({})
      call stalker#utils#merge_dict(exp1, exp2)
      continue
    endif
    
    " final override
    let a:expr1[key] = a:expr2[key]
  endfor
endfunction

function! stalker#utils#gen_dict(...) abort
  if a:0 == 1
    " final
    return a:1
  endif
  return { a:1: call('stalker#utils#gen_dict', a:000[1:]) }
endfunction

