function! s:get_nvim_version()
  redir => l:s
  silent! version
  redir END
  return matchstr(l:s, 'NVIM v\zs[^\n]*')
endfunction

let s:version_str = 'nvim ' . s:get_nvim_version()

let g:stalker#default#startify#header = [ ' [ stalker ' . g:stalker_version . ' @' . s:version_str . ' ]' ]

let g:stalker#default#startify#list_order = [
      \ [' Recent Files:'],
      \ 'files',
      \ [' Project:'],
      \ 'dir',
      \ [' Sessions:'],
      \ 'sessions',
      \ [' Bookmarks:'],
      \ 'bookmarks',
      \ [' Commands:'],
      \ 'commands',
      \ ]
