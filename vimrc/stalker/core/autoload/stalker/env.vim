scriptencoding utf-8


function! stalker#env#detect()
  " detect platform {{{

  let g:is_mac = has('macunix')
  let g:is_linux = has('unix') && !has('maxunix') && !has('win32unix')
  let g:is_windows = has('win32') || has('win64')

  " }}}

  " detect vim basic informations {{{

  let g:is_nvim = has('nvim') && exists('*jobwait') && !g:is_windows
  let g:is_vim8 = exists('*job_start')
  let g:has_timer = exists('*timer_start')
  let g:is_gui = has('gui_running')
  let g:tmux = !empty($TMUX)

  " }}}
endfunction
