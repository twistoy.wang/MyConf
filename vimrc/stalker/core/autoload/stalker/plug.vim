
" Install all plugs
function! stalker#plug#install_all() abort
  if !exists('g:stalk_plug_root')
    let g:stalk_plug_root = '~/.local/share/nvim/plugged'
  endif

  call plug#begin(g:stalk_plug_root)

  call s:plug_dep_from_components()
  call s:filter_plugs()
  call s:invoke_plugs()

  call plug#end()
endfunction

" source file
function! s:source_file(file) abort
  execute 'source ' . fnameescape(a:file)
endfunction

function! s:plug_dep_from_components() abort
  for l:com in g:component_enabled
    try
      let l:dep_file = g:component_available[l:com].dir . '/dep.vim'
      call stalker#utils#source_file(l:dep_file)
    catch
      call stalker#component#initialize()
    endtry
  endfor

  if exists('g:private')
    for l:private_components in g:private
      let l:dep_file = g:stalker_dir . '/private/' . l:private_components . '/dep.vim'
      if filereadable(expand(l:dep_file))
        call stalker#utils#source_file(l:dep_file)
      endif
    endfor
  endif

  let l:private_dep = g:stalker_dir . '/private/dep.vim'
  if filereadable(expand(l:private_dep))
    call stalker#utils#source_file(l:private_dep)
  endif
endfunction


function! s:filter_plugs() abort
  call filter(g:stalker_plugins, 'index(g:excluded_plugs, v:val) < 0')
endfunction

function! s:invoke_plugs() abort
  for l:plug in g:stalker_plugins
    call plug#(l:plug, get(g:plug_options, l:plug, ''))
  endfor
endfunction

