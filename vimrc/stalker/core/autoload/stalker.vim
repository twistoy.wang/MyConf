scriptencoding utf-8


" defind global variables {{{

let g:stalker = {}
let g:excluded_plugs = []

let g:component_available = {}
let g:component_enabled = []

let g:stalker_plugins = []
let g:plug_options = {}

let s:TYPE = {
      \  'string': type(''),
      \  'list': type([]),
      \  'dict': type({}),
      \  'funcref': type(function('call'))
      \}

let s:user_custom_config = $HOME . '/.custom.stalker'

call stalker#env#detect()

" }}}


function! stalker#begin() abort
  " check if vim-plug is installed
  let l:vim_plug_path = '~/.local/share/nvim/site/autoload/plug.vim'
  if empty(glob(l:vim_plug_path))
    call stalker#utils#download_vim_plug(l:vim_plug_path)
  endif

  " defind internal commands {{{

  command! -nargs=+ -bar UsePlug call s:RegisterPlugCommand(<args>)
  command! -nargs=+ -bar Component call s:RegisterComponent(<args>)

  " }}}

  call stalker#component#initialize()

  let g:mapleader = "\<Space>"
  let g:maplocalleader = ','

  " load user custom config {{{

  if filereadable(expand(s:user_custom_config))
    call stalker#utils#source_file(s:user_custom_config)

    if exists('g:stalker_enable_components')
      let g:component_enabled = g:component_enabled + g:stalker_enable_components
    endif

    let g:mapleader = get(g:, 'stalker_leader', "\<Space>")
    let g:maplocalleader = get(g:, 'stalker_local_leader', ',')

  endif

  " }}}

endfunction


function! stalker#end() abort
  call stalker#plug#install_all()

  if exists('*PreUserConfig')
    call PreUserConfig()
  endif

  silent! runtime! plugin/default.vim

  call stalker#component#load_configs()

  if exists('*UserConfig')
    call UserConfig()
  endif

  call stalker#utils#install_missing_plugs()
endfunction


function! s:RegisterPlugCommand(plug, ...) abort
  if index(g:stalker_plugins, a:plug) < 0
    call add(g:stalker_plugins, a:plug)
  endif

  if a:0 == 1
    let g:plug_options[a:plug] = a:1
  endif
endfunction

function! s:RegisterComponent(component, ...) abort
  if index(g:component_enabled, a:component) < 0
    call add(g:component_enabled, a:component)
  endif

  if a:0 == 1
    call s:parse_command_options(a:1)
  endif
endfunction

function! s:to_array(arg) abort
  return type(a:arg) == s:TYPE.list ? a:arg : [a:arg]
endfunction

function! s:parse_command_options(arg) abort
  let l:type = type(a:arg)
  if l:type == s:TYPE.dict
    if has_key(a:arg, 'exclude')
      for l:excl in s:to_array(a:arg['exclude'])
        call add(g:excluded_plugs, l:excl)
      endfor
    else
      throw 'Invalid options, expected field: exclude'
    endif
  else
    throw 'Invalid options, expect type: dict'
  endif
endfunction

